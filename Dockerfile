FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY ["IntroductionTaskPlayngo/IntroductionTaskPlayngo.csproj", "IntroductionTaskPlayngo/"]
RUN dotnet restore "IntroductionTaskPlayngo/IntroductionTaskPlayngo.csproj"
COPY . .
WORKDIR "/src/IntroductionTaskPlayngo"
RUN dotnet build "IntroductionTaskPlayngo.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "IntroductionTaskPlayngo.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "IntroductionTaskPlayngo.dll"]