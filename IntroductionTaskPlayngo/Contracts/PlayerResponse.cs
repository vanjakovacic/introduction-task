﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IntroductionTaskPlayngo.Contracts
{
    public class PlayerResponse
    {
        /// <summary>
        /// Unique identifier for the player.
        /// </summary>
        [JsonProperty("player_id")]
        public int PlayerId { get; set; }

        /// <summary>
        /// Name of the player.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Surname of the player.
        /// </summary>
        [JsonProperty("surname")]
        public string Surname { get; set; }

        /// <summary>
        /// Date of birth of the player in the following format: "yyyy-MM-dd".
        /// </summary>
        [JsonProperty("birth_date")]
        public string BirthDate { get; set; }

        /// <summary>
        /// Name of the country where the player was born.
        /// </summary>
        [JsonProperty("country")]
        public string Country { get; set; }

        /// <summary>
        /// Name of the position that the player usually plays on.
        /// </summary>
        [JsonProperty("position")]
        public string Position { get; set; }

        /// <summary>
        /// The market value of the player in millions of euros.
        /// </summary>
        [JsonProperty("market_value")]
        public int MarketValue { get; set; }

        /// <summary>
        /// Expensive player is true, when the player has a market value over 100.
        /// </summary>
        [JsonProperty("expensive_player")]
        public bool ExpensivePlayer { get; set; }

        /// <summary>
        /// Young player is true, when the player is younger than 28 years old.
        /// </summary>
        [JsonProperty("young_player")]
        public bool YoungPlayer { get; set; }
    }
}
