﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IntroductionTaskPlayngo.Contracts
{
    public class PlayerRequest
    {
	    /// <summary>
	    /// Name of the player.
	    /// </summary>
	    [JsonProperty("name")]
		public string Name { get; set; }

	    /// <summary>
	    /// Surname of the player.
	    /// </summary>
	    [JsonProperty("surname")]
		public string Surname { get; set; }

	    /// <summary>
	    /// Date of birth of the player.
	    /// </summary>
	    [JsonProperty("birth_date")]
		public DateTime BirthDate { get; set; }

		/// <summary>
		/// Country identifier.
		/// </summary>
		[JsonProperty("country_id")]
        public int CountryId { get; set; }

		/// <summary>
		/// Position identifier.
		/// </summary>
		[JsonProperty("position_id")]
        public int PositionId { get; set; }

	    /// <summary>
	    /// The market value of the player in millions of euros.
	    /// </summary>
	    [JsonProperty("market_value")]
		public int MarketValue { get; set; }
    }
}
