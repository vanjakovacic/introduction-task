﻿using Dapper.FluentMap;
using IntroductionTaskPlayngo.Contracts;
using IntroductionTaskPlayngo.Helpers;
using IntroductionTaskPlayngo.Mappers;
using IntroductionTaskPlayngo.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing.Template;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;

namespace IntroductionTaskPlayngo
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            FluentMapper.Initialize(config =>
            {
                config.AddMap(new PlayerMap());
            });

            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Introduction Task API", Version = "v1" });
				c.IncludeXmlComments(AppContext.BaseDirectory + "/IntroductionTaskPlayngo.xml");
			});

            services.AddTransient<IPlayerService, PlayerService>();
            services.AddTransient<IHelper, Helper>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Introduction Task API v1");
            });

            app.UseMvc();
        }
    }
}
