﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using IntroductionTaskPlayngo.Contracts;
using IntroductionTaskPlayngo.Services;
using Microsoft.AspNetCore.Mvc;

namespace IntroductionTaskPlayngo.Controllers
{
    /// <summary>
    /// API controller
    /// </summary>
    [Route("IntroductionTaskAPI")]
    public class IntroductionTaskApiController : Controller
    {
        private readonly IPlayerService _playerService;

        public IntroductionTaskApiController(IPlayerService playerService)
        {
            _playerService = playerService;
        }

        [HttpGet]
        [Route("getallplayers")]
        public List<PlayerResponse> GetAllPlayers()
        {
            return _playerService.GetAllPlayers();
        }

        [HttpGet]
        [Route("getplayerbyid")]
        public PlayerResponse GetPlayerById(int id)
        {
            return _playerService.GetPlayerById(id);
        }

        [HttpGet]
        [Route("getplayersbycountry")]
        public List<PlayerResponse> GetPlayersByCountry(int id)
        {
            return _playerService.GetPlayersByCountry(id).ToList();
        }

        [HttpGet]
        [Route("getplayersbyposition")]
        public List<PlayerResponse> GetPlayersByPosition(int id)
        {
            return _playerService.GetPlayersByPosition(id).ToList();
        }

        [HttpPut]
        [Route("addplayer")]
        public void AddPlayer([FromBody] PlayerRequest player)
        {
            _playerService.AddPlayer(player);
        }
    }
}