﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntroductionTaskPlayngo.Contracts;

namespace IntroductionTaskPlayngo.Helpers
{
    public interface IHelper
    {
        bool IsExpensivePlayer(PlayerResponse player);

        bool IsYoungPlayer(PlayerResponse player);
    }
}
