﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntroductionTaskPlayngo.Contracts;

namespace IntroductionTaskPlayngo.Helpers
{
    public class Helper : IHelper
    {
        public bool IsExpensivePlayer(PlayerResponse player)
        {
	        return player.MarketValue > 100;
        }

        public bool IsYoungPlayer(PlayerResponse player)
        {
            var days = (DateTime.Now - Convert.ToDateTime(player.BirthDate)).Days;

            var years = days / 365.25m;

            return years < 28;
        }
    }
}
