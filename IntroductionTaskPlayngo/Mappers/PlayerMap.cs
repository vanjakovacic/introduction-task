﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper.FluentMap.Mapping;
using IntroductionTaskPlayngo.Contracts;

namespace IntroductionTaskPlayngo.Mappers
{
    public class PlayerMap : EntityMap<PlayerResponse>
    {
        public PlayerMap()
        {
            Map(i => i.PlayerId).ToColumn("player_id");
            Map(i => i.BirthDate).ToColumn("birth_date");
            Map(i => i.MarketValue).ToColumn("market_value");
        }
    }
}
