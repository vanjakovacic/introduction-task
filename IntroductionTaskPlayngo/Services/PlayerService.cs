﻿using System;
using Dapper;
using IntroductionTaskPlayngo.Contracts;
using IntroductionTaskPlayngo.Helpers;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace IntroductionTaskPlayngo.Services
{
	public class PlayerService : IPlayerService
	{
		private readonly IDbConnection _db;
		private readonly IHelper _helper;

		public PlayerService(IHelper helper)
		{
			_db = new SqlConnection(Environment.GetEnvironmentVariable("CONNECTION_STRING"));
			_helper = helper;
		}

		public List<PlayerResponse> GetAllPlayers()
		{
			var playerList = _db.Query<PlayerResponse>("players_get_all", commandType: CommandType.StoredProcedure).ToList();

			//Just for Unit test purposes.
			foreach (PlayerResponse player in playerList)
			{
				player.ExpensivePlayer = _helper.IsExpensivePlayer(player);
				player.YoungPlayer = _helper.IsYoungPlayer(player);
			}

			return playerList;
		}

		public PlayerResponse GetPlayerById(int playerId)
		{
			var player = _db.Query<PlayerResponse>("players_get_by_id", new { player_id = playerId }, commandType: CommandType.StoredProcedure).SingleOrDefault();

			//Just for Unit test purposes.
			if (player != null)
			{
				player.ExpensivePlayer = _helper.IsExpensivePlayer(player);
				player.YoungPlayer = _helper.IsYoungPlayer(player);
			}

			return player;
		}

		public List<PlayerResponse> GetPlayersByCountry(int countryId)
		{
			var playerList = _db.Query<PlayerResponse>("players_get_by_country", new { country_id = countryId }, commandType: CommandType.StoredProcedure).ToList();

			//Just for Unit test purposes
			foreach (PlayerResponse player in playerList)
			{
				player.ExpensivePlayer = _helper.IsExpensivePlayer(player);
				player.YoungPlayer = _helper.IsYoungPlayer(player);
			}

			return playerList;
		}

		public List<PlayerResponse> GetPlayersByPosition(int positionId)
		{
			var playerList = _db.Query<PlayerResponse>("players_get_by_position", new { position_id = positionId }, commandType: CommandType.StoredProcedure).ToList();

			//Just for Unit test purposes
			foreach (var player in playerList)
			{
				player.ExpensivePlayer = _helper.IsExpensivePlayer(player);
				player.YoungPlayer = _helper.IsYoungPlayer(player);
			}

			return playerList;
		}

		public void AddPlayer(PlayerRequest player)
		{
			var parameters = new DynamicParameters();

			parameters.Add("@name", player.Name, dbType: DbType.String, ParameterDirection.Input);
			parameters.Add("@surname", player.Surname, dbType: DbType.String, ParameterDirection.Input);
			parameters.Add("@birth_date", player.BirthDate, dbType: DbType.Date, ParameterDirection.Input);
			parameters.Add("@country_id", player.CountryId, dbType: DbType.Int32, ParameterDirection.Input);
			parameters.Add("@position_id", player.PositionId, dbType: DbType.Int32, ParameterDirection.Input);
			parameters.Add("@market_value", player.MarketValue, dbType: DbType.Int32, ParameterDirection.Input);

			_db.Execute("players_add", parameters, commandType: CommandType.StoredProcedure);
		}
	}
}
