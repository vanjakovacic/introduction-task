﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntroductionTaskPlayngo.Contracts;

namespace IntroductionTaskPlayngo.Services
{
    public interface IPlayerService
    {
        List<PlayerResponse> GetAllPlayers();

        PlayerResponse GetPlayerById(int playerId);

        List<PlayerResponse> GetPlayersByCountry(int countryId);

        List<PlayerResponse> GetPlayersByPosition(int positionId);

        void AddPlayer(PlayerRequest player);
    }
}
