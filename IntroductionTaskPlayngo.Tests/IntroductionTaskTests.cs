using System;
using IntroductionTaskPlayngo.Contracts;
using IntroductionTaskPlayngo.Controllers;
using IntroductionTaskPlayngo.Helpers;
using IntroductionTaskPlayngo.Services;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Utilities;
using Moq;
using Xunit;

namespace IntroductionTaskPlayngo.Tests
{
    public class IntroductionTaskTests
    {
        private readonly Helper _helper = new Helper();
	    private readonly Mock<PlayerResponse> _mockPlayer = new Mock<PlayerResponse>();

        [Fact]
        public void IsYoungPlayer_FalseIfOlderThanTwentyEightYears()
        {
            _mockPlayer.Object.BirthDate = "1980-01-01";

            var result = _helper.IsYoungPlayer(_mockPlayer.Object);

            Assert.False(result);
        }

        [Fact]
        public void IsYoungPlayer_TrueIfYoungerThanTwentyEightYears()
        {
            _mockPlayer.Object.BirthDate = "1999-01-01";

            var result = _helper.IsYoungPlayer(_mockPlayer.Object);

            Assert.True(result);
        }

        [Fact]
        public void IsExpensivePlayer_FalseIfMarketValueIsUnderOneHundred()
        {
            _mockPlayer.Object.MarketValue = 8;

            var result = _helper.IsExpensivePlayer(_mockPlayer.Object);

            Assert.False(result);
        }

        [Fact]
        public void IsExpensivePlayer_TrueIfMarketValueIsOverOneHundred()
        {
            _mockPlayer.Object.MarketValue = 150;

            var result = _helper.IsExpensivePlayer(_mockPlayer.Object);

            Assert.True(result);
        }
    }
}
