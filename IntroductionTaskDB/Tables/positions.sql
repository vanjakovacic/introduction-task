﻿CREATE TABLE [dbo].[positions]
(
	[position_id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [name] VARCHAR(50) NOT NULL
)
