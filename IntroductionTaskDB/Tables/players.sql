﻿CREATE TABLE [dbo].[players]
(
	[player_id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [name] VARCHAR(50) NOT NULL, 
    [surname] VARCHAR(50) NOT NULL, 
    [birth_date] DATE NOT NULL, 
    [country_id] INT NOT NULL, 
    [position_id] INT NOT NULL, 
    [market_value] INT NOT NULL, 
    CONSTRAINT [FK_players_countries] FOREIGN KEY ([country_id]) REFERENCES [countries]([country_id]), 
    CONSTRAINT [FK_players_positions] FOREIGN KEY ([position_id]) REFERENCES [positions]([position_id])
)
