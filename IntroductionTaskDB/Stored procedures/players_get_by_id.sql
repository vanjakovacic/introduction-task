﻿CREATE PROCEDURE [dbo].[players_get_by_id] 
	@player_id int
AS
BEGIN

	SET NOCOUNT ON;

		SELECT        players.player_id, players.name, players.surname, players.birth_date, positions.name AS position, countries.name AS country, players.market_value
FROM            countries INNER JOIN
                         players ON countries.country_id = players.country_id INNER JOIN
                         positions ON players.position_id = positions.position_id
						 WHERE players.player_id = @player_id
END