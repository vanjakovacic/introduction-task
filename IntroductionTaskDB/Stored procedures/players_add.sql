﻿CREATE PROCEDURE [dbo].[players_add]
(
	@name VARCHAR(50),
	@surname VARCHAR(50),
	@birth_date DATE,
	@country_id INT,
	@position_id INT,
	@market_value INT
	)
AS
BEGIN

	SET NOCOUNT ON;

INSERT INTO [dbo].[players]
           ([name]
           ,[surname]
           ,[birth_date]
           ,[country_id]
           ,[position_id]
           ,[market_value])
     VALUES
           (@name
           ,@surname
           ,@birth_date
           ,@country_id
           ,@position_id
           ,@market_value)
END
