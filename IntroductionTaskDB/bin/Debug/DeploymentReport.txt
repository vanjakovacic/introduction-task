﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Create
       [dbo].[countries] (Table)
       [dbo].[players] (Table)
       [dbo].[positions] (Table)
       [dbo].[FK_players_countries] (Foreign Key)

** Supporting actions
